﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RT_Botting
{
    public static class Str
    {
        public static string StripHTML(string input)
        {
            return Regex.Replace(input.Replace("<br/>", "\n"), "<.*?>", String.Empty);
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string TranslateError(string input)
        {
            //You cannot write comments on this site
            if (input.Contains("Вы не можете писать комментарии на этом сайте"))
                    return "This account has been banned";
            //Rating limit reached
            if (input.Contains("Достигнут лимит на кол-во оценок"))
                    return "Rate limit reached. Please cancel and wait for a while";
            if (input.Contains("Нельзя ставить оценки своим комментариям"))
                    return "Can't rate your comments";
            else
                return "";
        }
    }
}
