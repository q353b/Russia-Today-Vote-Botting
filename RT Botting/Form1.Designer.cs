﻿namespace RT_Botting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.editTokens = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.upvote = new System.Windows.Forms.RadioButton();
            this.downvote = new System.Windows.Forms.RadioButton();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.massVoteCommBtn = new System.Windows.Forms.Button();
            this.startBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.loadComms = new System.Windows.Forms.Button();
            this.commListView = new System.Windows.Forms.ListView();
            this.dateTimeCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.userIdCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.userCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.votesCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.commsCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.commIdCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PasteBtn = new System.Windows.Forms.Button();
            this.goBackBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.logTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.commentViewer = new System.Windows.Forms.RichTextBox();
            this.rtUrl = new System.Windows.Forms.TextBox();
            this.setDelay = new System.Windows.Forms.CheckBox();
            this.setMaxVote = new System.Windows.Forms.CheckBox();
            this.randVotes = new System.Windows.Forms.CheckBox();
            this.maxVote = new System.Windows.Forms.NumericUpDown();
            this.delay = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxVote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delay)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tokens:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.setDelay);
            this.groupBox1.Controls.Add(this.setMaxVote);
            this.groupBox1.Controls.Add(this.editTokens);
            this.groupBox1.Controls.Add(this.randVotes);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.maxVote);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.upvote);
            this.groupBox1.Controls.Add(this.delay);
            this.groupBox1.Controls.Add(this.downvote);
            this.groupBox1.Location = new System.Drawing.Point(763, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 246);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // editTokens
            // 
            this.editTokens.Location = new System.Drawing.Point(61, 19);
            this.editTokens.Name = "editTokens";
            this.editTokens.Size = new System.Drawing.Size(136, 23);
            this.editTokens.TabIndex = 5;
            this.editTokens.Text = "Edit text";
            this.editTokens.UseVisualStyleBackColor = true;
            this.editTokens.Click += new System.EventHandler(this.EditTokens_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Increase if rate limited";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Voting:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(173, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "ms.";
            // 
            // upvote
            // 
            this.upvote.AutoSize = true;
            this.upvote.Checked = true;
            this.upvote.ForeColor = System.Drawing.Color.ForestGreen;
            this.upvote.Location = new System.Drawing.Point(139, 50);
            this.upvote.Name = "upvote";
            this.upvote.Size = new System.Drawing.Size(62, 17);
            this.upvote.TabIndex = 2;
            this.upvote.TabStop = true;
            this.upvote.Text = "Upvote";
            this.upvote.UseVisualStyleBackColor = true;
            // 
            // downvote
            // 
            this.downvote.AutoSize = true;
            this.downvote.BackColor = System.Drawing.Color.Transparent;
            this.downvote.ForeColor = System.Drawing.Color.Firebrick;
            this.downvote.Location = new System.Drawing.Point(58, 50);
            this.downvote.Name = "downvote";
            this.downvote.Size = new System.Drawing.Size(78, 17);
            this.downvote.TabIndex = 2;
            this.downvote.Text = "Downvote";
            this.downvote.UseVisualStyleBackColor = false;
            // 
            // cancelBtn
            // 
            this.cancelBtn.ForeColor = System.Drawing.Color.Red;
            this.cancelBtn.Location = new System.Drawing.Point(665, 66);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 6;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // massVoteCommBtn
            // 
            this.massVoteCommBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.massVoteCommBtn.Location = new System.Drawing.Point(376, 66);
            this.massVoteCommBtn.Name = "massVoteCommBtn";
            this.massVoteCommBtn.Size = new System.Drawing.Size(177, 23);
            this.massVoteCommBtn.TabIndex = 3;
            this.massVoteCommBtn.Text = "Mass voting a comment";
            this.massVoteCommBtn.UseVisualStyleBackColor = true;
            this.massVoteCommBtn.Click += new System.EventHandler(this.MassVoteComment_Click);
            // 
            // startBtn
            // 
            this.startBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startBtn.Location = new System.Drawing.Point(176, 66);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(194, 23);
            this.startBtn.TabIndex = 3;
            this.startBtn.Text = "Mass voting on user profile";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.MassVoteProfile_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(148, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Full article url or profile ID: ";
            // 
            // loadComms
            // 
            this.loadComms.Location = new System.Drawing.Point(665, 19);
            this.loadComms.Name = "loadComms";
            this.loadComms.Size = new System.Drawing.Size(75, 23);
            this.loadComms.TabIndex = 7;
            this.loadComms.Text = "Load";
            this.loadComms.UseVisualStyleBackColor = true;
            this.loadComms.Click += new System.EventHandler(this.LoadComments_Click);
            // 
            // commListView
            // 
            this.commListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dateTimeCol,
            this.userIdCol,
            this.userCol,
            this.votesCol,
            this.commsCol,
            this.commIdCol});
            this.commListView.FullRowSelect = true;
            this.commListView.HideSelection = false;
            this.commListView.Location = new System.Drawing.Point(7, 95);
            this.commListView.Name = "commListView";
            this.commListView.Size = new System.Drawing.Size(734, 269);
            this.commListView.TabIndex = 8;
            this.commListView.UseCompatibleStateImageBehavior = false;
            this.commListView.View = System.Windows.Forms.View.Details;
            this.commListView.SelectedIndexChanged += new System.EventHandler(this.viewComment_SelectedIndexChanged);
            this.commListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.viewProfileComments_MouseDoubleClick);
            // 
            // dateTimeCol
            // 
            this.dateTimeCol.Text = "Date & time";
            this.dateTimeCol.Width = 95;
            // 
            // userIdCol
            // 
            this.userIdCol.Text = "User ID";
            this.userIdCol.Width = 54;
            // 
            // userCol
            // 
            this.userCol.Text = "User";
            this.userCol.Width = 110;
            // 
            // votesCol
            // 
            this.votesCol.Text = "👍";
            this.votesCol.Width = 32;
            // 
            // commsCol
            // 
            this.commsCol.Text = "Comment";
            this.commsCol.Width = 345;
            // 
            // commIdCol
            // 
            this.commIdCol.Text = "Comment ID";
            this.commIdCol.Width = 77;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(137, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(475, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Tip: Double click to view profile comments. Hold SHIFT or CTRL to select multiple" +
    " comments";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLbl,
            this.progressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 500);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(976, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLbl
            // 
            this.statusLbl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusLbl.Image = ((System.Drawing.Image)(resources.GetObject("statusLbl.Image")));
            this.statusLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.statusLbl.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.statusLbl.Name = "statusLbl";
            this.statusLbl.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.statusLbl.Size = new System.Drawing.Size(859, 17);
            this.statusLbl.Spring = true;
            this.statusLbl.Text = "Ready";
            this.statusLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressBar
            // 
            this.progressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.progressBar.AutoSize = false;
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 16);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PasteBtn);
            this.groupBox2.Controls.Add(this.goBackBtn);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.loadComms);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.commListView);
            this.groupBox2.Controls.Add(this.rtUrl);
            this.groupBox2.Controls.Add(this.cancelBtn);
            this.groupBox2.Controls.Add(this.startBtn);
            this.groupBox2.Controls.Add(this.massVoteCommBtn);
            this.groupBox2.Location = new System.Drawing.Point(8, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(746, 370);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Comments list (Supported sites: rt.com, russian.rt.com and arabic.rt.com)";
            // 
            // PasteBtn
            // 
            this.PasteBtn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasteBtn.Location = new System.Drawing.Point(628, 19);
            this.PasteBtn.Name = "PasteBtn";
            this.PasteBtn.Size = new System.Drawing.Size(31, 23);
            this.PasteBtn.TabIndex = 11;
            this.PasteBtn.Text = "📋";
            this.PasteBtn.UseVisualStyleBackColor = true;
            this.PasteBtn.Click += new System.EventHandler(this.PasteBtn_Click);
            // 
            // goBackBtn
            // 
            this.goBackBtn.Enabled = false;
            this.goBackBtn.Location = new System.Drawing.Point(7, 66);
            this.goBackBtn.Name = "goBackBtn";
            this.goBackBtn.Size = new System.Drawing.Size(75, 23);
            this.goBackBtn.TabIndex = 10;
            this.goBackBtn.Text = "Go back";
            this.goBackBtn.UseVisualStyleBackColor = true;
            this.goBackBtn.Click += new System.EventHandler(this.GoBackBtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.logTextBox);
            this.groupBox3.Location = new System.Drawing.Point(8, 381);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(746, 116);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log";
            // 
            // logTextBox
            // 
            this.logTextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logTextBox.Location = new System.Drawing.Point(6, 20);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.Size = new System.Drawing.Size(734, 90);
            this.logTextBox.TabIndex = 0;
            this.logTextBox.Text = "";
            this.logTextBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.logTextBox_LinkClicked);
            this.logTextBox.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.commentViewer);
            this.groupBox4.Location = new System.Drawing.Point(763, 257);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 240);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Comment viewer";
            // 
            // commentViewer
            // 
            this.commentViewer.Location = new System.Drawing.Point(6, 16);
            this.commentViewer.Name = "commentViewer";
            this.commentViewer.ReadOnly = true;
            this.commentViewer.Size = new System.Drawing.Size(191, 218);
            this.commentViewer.TabIndex = 0;
            this.commentViewer.Text = "";
            // 
            // rtUrl
            // 
            this.rtUrl.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::RT_Botting.Properties.Settings.Default, "RtUrl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rtUrl.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.rtUrl.Location = new System.Drawing.Point(160, 19);
            this.rtUrl.Name = "rtUrl";
            this.rtUrl.Size = new System.Drawing.Size(462, 22);
            this.rtUrl.TabIndex = 6;
            this.rtUrl.Text = global::RT_Botting.Properties.Settings.Default.RtUrl;
            // 
            // setDelay
            // 
            this.setDelay.AutoSize = true;
            this.setDelay.Checked = global::RT_Botting.Properties.Settings.Default.SetDelay;
            this.setDelay.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::RT_Botting.Properties.Settings.Default, "SetDelay", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.setDelay.Location = new System.Drawing.Point(13, 134);
            this.setDelay.Name = "setDelay";
            this.setDelay.Size = new System.Drawing.Size(57, 17);
            this.setDelay.TabIndex = 14;
            this.setDelay.Text = "Delay:";
            this.setDelay.UseVisualStyleBackColor = true;
            // 
            // setMaxVote
            // 
            this.setMaxVote.AutoSize = true;
            this.setMaxVote.Checked = global::RT_Botting.Properties.Settings.Default.SetMaxVoting;
            this.setMaxVote.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::RT_Botting.Properties.Settings.Default, "SetMaxVoting", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.setMaxVote.Location = new System.Drawing.Point(13, 79);
            this.setMaxVote.Name = "setMaxVote";
            this.setMaxVote.Size = new System.Drawing.Size(86, 17);
            this.setMaxVote.TabIndex = 13;
            this.setMaxVote.Text = "Max voting:";
            this.setMaxVote.UseVisualStyleBackColor = true;
            // 
            // randVotes
            // 
            this.randVotes.AutoSize = true;
            this.randVotes.Checked = global::RT_Botting.Properties.Settings.Default.RandomizeVoting;
            this.randVotes.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::RT_Botting.Properties.Settings.Default, "RandomizeVoting", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.randVotes.Location = new System.Drawing.Point(13, 107);
            this.randVotes.Name = "randVotes";
            this.randVotes.Size = new System.Drawing.Size(119, 17);
            this.randVotes.TabIndex = 11;
            this.randVotes.Text = "Randomize voting";
            this.randVotes.UseVisualStyleBackColor = true;
            // 
            // maxVote
            // 
            this.maxVote.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::RT_Botting.Properties.Settings.Default, "MaxVoting", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.maxVote.Location = new System.Drawing.Point(107, 77);
            this.maxVote.Name = "maxVote";
            this.maxVote.Size = new System.Drawing.Size(58, 22);
            this.maxVote.TabIndex = 12;
            this.maxVote.Value = global::RT_Botting.Properties.Settings.Default.MaxVoting;
            // 
            // delay
            // 
            this.delay.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::RT_Botting.Properties.Settings.Default, "Delay", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.delay.Location = new System.Drawing.Point(107, 132);
            this.delay.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.delay.Name = "delay";
            this.delay.Size = new System.Drawing.Size(58, 22);
            this.delay.TabIndex = 8;
            this.delay.Value = global::RT_Botting.Properties.Settings.Default.Delay;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 522);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Russia Today Vote Botting v1.4 | VanHoevenTR";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maxVote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton downvote;
        private System.Windows.Forms.RadioButton upvote;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button editTokens;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox rtUrl;
        private System.Windows.Forms.Button loadComms;
        private System.Windows.Forms.ListView commListView;
        private System.Windows.Forms.ColumnHeader userCol;
        private System.Windows.Forms.ColumnHeader commsCol;
        private System.Windows.Forms.ColumnHeader votesCol;
        private System.Windows.Forms.ColumnHeader userIdCol;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.StatusStrip statusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel statusLbl;
        internal System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.Button massVoteCommBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox logTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown delay;
        private System.Windows.Forms.CheckBox randVotes;
        private System.Windows.Forms.CheckBox setMaxVote;
        private System.Windows.Forms.NumericUpDown maxVote;
        private System.Windows.Forms.Button goBackBtn;
        private System.Windows.Forms.ColumnHeader dateTimeCol;
        private System.Windows.Forms.ColumnHeader commIdCol;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RichTextBox commentViewer;
        private System.Windows.Forms.Button PasteBtn;
        private System.Windows.Forms.CheckBox setDelay;
    }
}

