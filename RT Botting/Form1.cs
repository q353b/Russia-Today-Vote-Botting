﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace RT_Botting
{
    public partial class Form1 : Form
    {
        string userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36";
        string exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        string tokensFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "tokens.txt");
        string historyFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "history.txt");
        string previousUrl;

        bool isRunning, isCancelled;

        CookieContainer cookieContainer = new CookieContainer();

        public Form1()
        {
            InitializeComponent();
            CheckUpdate();

            //rtUrl.Click += delegate
            //{
            //    rtUrl.SelectAll();
            //};
        }

        #region Fun stuff
        string[] loadToken()
        {
            if (File.Exists(tokensFilePath))
            {
                var tokens = File.ReadAllLines(tokensFilePath).OrderBy(line => Guid.NewGuid()).ToArray();
                var rnd = new Random();
                tokens = tokens.OrderBy(line => rnd.Next()).ToArray();
                var newTokens = new List<string>();

                foreach (string t in tokens)
                {
                    if (t.Contains("#"))
                        continue;
                    newTokens.Add(t);
                }

                return newTokens.ToArray();
            }
            return null;
        }

        private Vote VoteMode()
        {
            if (downvote.Checked == true)
                return Vote.down;
            return Vote.up;
        }

        private string getSiteId(string url)
        {
            if (url.Contains("russian.rt.com/inotv"))
                return "3734";
            else if(url.Contains("russian.rt.com"))
                return "3724";
            else if (url.Contains("arabic.rt.com"))
                return "3725";
            else
                return "4194";
        }
        #endregion

        #region Click handler
        private async void MassVoteProfile_Click(object sender, EventArgs e)
        {
            try
            {
                string userId = null;
                if (commListView.Items.Count > 0)
                {
                    for (int i = 0; i < commListView.Items.Count; i++)
                    {
                        if (commListView.Items[i].Selected == true)
                        {
                            userId = commListView.Items[i].SubItems[1].Text;
                        }
                    }
                }

                if (String.IsNullOrEmpty(userId))
                {
                    MessageBox.Show("No comment selected", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    startBtn.Enabled = true;
                    Done("Error: No comment selected");
                    return;
                }

                string[] tokens = loadToken();

                string userComms = null;
                await Task.Factory.StartNew(() =>
                {
                    userComms = WebGET("https://web.tolstoycomments.com/api/user/profile?userid=" + userId + "&siteid=4194&token=" + HttpUtility.UrlEncode(tokens[0], UnicodeEncoding.UTF8) + "&lang=en");
                });
                var rnd = new Random();

                var rtcomms = JsonConvert.DeserializeObject<ProfileComments.Root>(userComms);
                var comments = rtcomms.Data.Comments;
                foreach (var c in comments)
                {
                    if (isCancelled)
                        break;

                    tokens = tokens.OrderBy(line => rnd.Next()).ToArray();

                    Run($"Voting {Array.IndexOf(comments, c) + 1} (ID: {c.Id}) out of {comments.Length}");
                    await Task.Factory.StartNew(() =>
                    {
                        MassVote(tokens, c.Id.ToString());
                    });
                }
                Done();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured\n" + ex.Message);
                Done("An error occured: " + ex.Message);
            }
        }

        private async void MassVoteComment_Click(object sender, EventArgs e)
        {
            try
            {
                bool selected = false;
                var commIds = new List<string>();
                if (commListView.Items.Count > 0)
                {
                    for (int i = 0; i < commListView.Items.Count; i++)
                    {
                        if (commListView.Items[i].Selected == true)
                        {
                            commIds.Add(commListView.Items[i].SubItems[5].Text);
                            selected = true;
                        }
                    }
                }

                if (selected == false)
                {
                    MessageBox.Show("No comment selected", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    startBtn.Enabled = true;
                    Done();
                    return;
                }

                string[] tokens = loadToken();

                foreach (string id in commIds)
                {
                    if (isCancelled)
                        break;

                    Run($"Voting comment: {id}");

                    var rnd = new Random();
                    tokens = tokens.OrderBy(line => rnd.Next()).ToArray();

                    await Task.Factory.StartNew(() =>
                    {
                        MassVote(tokens, id);
                    });
                }

                Done();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured\n" + ex.Message);
                Done("An error occured: " + ex.Message);
            }
        }

        private void MassVote(string[] tokens, string commsId)
        {
            SaveHistory(commsId);
            string response = null;

            int numToStop = Num.Rand(1, setMaxVote.Checked ? (int)maxVote.Value : tokens.Length);

            for (int i = 0; i < tokens.Length; i++)
            {
                if (numToStop == i && randVotes.Checked)
                    break;

                if (maxVote.Value == i && setMaxVote.Checked)
                    break;

                if (isCancelled)
                    break;

                try
                {
                    response = VotePost(commsId, VoteMode(), tokens[i]);
                    if (VoteMode() == Vote.up)
                        Log($"Upvoted with token {Str.Truncate(tokens[i], 15)}...");
                    else
                        Log($"Downvoted with token {Str.Truncate(tokens[i], 15)}...");

                    if (setDelay.Checked)
                        Thread.Sleep(Convert.ToInt32(delay.Value));
                }
                catch (WebException ex)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        Log($"Error voting comment {commsId} with token {Str.Truncate(tokens[i], 15)}... {Str.TranslateError(reader.ReadToEnd())}");
                    }
                }
            }
        }

        private void EditTokens_Click(object sender, EventArgs e)
        {
            if (!File.Exists(Path.Combine(exePath, "tokens.txt")))
                File.WriteAllText(Path.Combine(exePath, "tokens.txt"), "# Add tokens each lines");
            Process.Start("explorer.exe", Path.Combine(exePath, "tokens.txt"));
        }

        private void LoadComments_Click(object sender, EventArgs e)
        {
            string[] tokens = loadToken();

            string url = "https://web.tolstoycomments.com/api/chatpage/first?siteid=" + getSiteId(rtUrl.Text) + "&hash=null&url=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(rtUrl.Text), Encoding.UTF8) + "&sort=1&format=1";
            if (!rtUrl.Text.Contains("rt.com"))
            {
                LoadProfileComments(rtUrl.Text);
                return;
            }

            LoadComments(url);
            previousUrl = url;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            if (isRunning)
                isCancelled = true;
        }

        private void GoBackBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(previousUrl))
                LoadComments(previousUrl);
            goBackBtn.Enabled = false;
        }

        private void PasteBtn_Click(object sender, EventArgs e)
        {
            rtUrl.Text = Clipboard.GetText(TextDataFormat.Text);
        }
        #endregion

        #region Load comments
        private async void LoadComments(string url)
        {
            Run("Loading comments...");
            try
            {
                commListView.Items.Clear();

            LoadMore:
                string comms = null;
                string sort = null;
                await Task.Factory.StartNew(() =>
                {
                    comms = WebGET(url);
                });

                var rtcomms = JsonConvert.DeserializeObject<ArticleComments.Root>(comms);
                var commentsList = rtcomms.Data.Comments;
                var commCounts = rtcomms.Data.Chat.CountCommentLoad;

                string[] historyList = null;
                try
                {
                    if (File.Exists(historyFilePath))
                        historyList = File.ReadAllLines(historyFilePath);
                }
                catch
                {
                    //Skip reading history
                }

                foreach (var c in commentsList)
                {
                    if (isCancelled)
                        break;

                    try
                    {
                        AddToList(historyList, c.DataCreate, c.User.Id, c.User.Name, c.Raiting.Val, c.TextTemplate, c.Id);
                        sort = c.Sort;
                        try
                        {
                            if (c.AnswerCommentCount == 1)
                            {
                                AddToList(historyList, c.AnswerComment.DataCreate, c.AnswerComment.User.Id, c.AnswerComment.User.Name, c.AnswerComment.Raiting.Val, "   @" + c.AnswerComment.TextTemplate, c.AnswerComment.Id);
                            }
                            else if (c.AnswerCommentCount > 1)
                            {
                                string anscomms = null;
                                await Task.Factory.StartNew(() =>
                                {
                                    anscomms = WebGET("https://web.tolstoycomments.com/api/chatpage/first?siteid=" + getSiteId(rtUrl.Text) + "&hash=" + rtcomms.Data.Chat.Hash + "&url=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(rtUrl.Text), Encoding.UTF8) + "&rootid=" + c.Id + "&sort=1&format=1");
                                });

                                var rtanscomms = JsonConvert.DeserializeObject<ArticleComments.Root>(anscomms);
                                var ansCommentsList = rtanscomms.Data.Comments;
                                for (int i = ansCommentsList.Length - 2; i > 1; i--)
                                {
                                    var cc = ansCommentsList[i];
                                    AddToList(historyList, cc.DataCreate, cc.User.Id, cc.User.Name, cc.Raiting.Val, "   @" + cc.TextTemplate, cc.Id);
                                    sort = c.Sort;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
                if (commCounts == 50 && !isCancelled && !String.IsNullOrEmpty(sort))
                {
                    url = "https://web.tolstoycomments.com/api/chatpage/page?siteid=" + getSiteId(rtUrl.Text) + "&hash=null&url=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(rtUrl.Text), Encoding.UTF8) + "&page=" + sort + "&down=true&sort=1&format=1";
                    goto LoadMore;
                }

                Done();
            }
            catch (Exception ex)
            {
                Done("An error occured: " + ex.Message);
                Debug.WriteLine(ex);
            }
        }

        private async void LoadProfileComments(string userId)
        {
            try
            {
                string[] tokens = loadToken();
                if (tokens == null)
                {
                    MessageBox.Show("Profile comments cannot be loaded without a token", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                string url = "https://web.tolstoycomments.com/api/user/profile?userid=" + userId + "&siteid=4194&token=" + HttpUtility.UrlEncode(tokens[0], UnicodeEncoding.UTF8) + "&lang=en";

                string comms = null;
                await Task.Factory.StartNew(() =>
                {
                    comms = WebGET(url);
                });

                var usrComms = JsonConvert.DeserializeObject<ProfileComments.Root>(comms);
                var comments = usrComms.Data.Comments;

                commListView.Items.Clear();
                isCancelled = false;

                var historyList = File.ReadAllLines(historyFilePath);

                foreach (var c in comments)
                {
                    if (isCancelled)
                        break;

                    try
                    {
                        AddToList(historyList, c.DataCreate, c.User.Id, c.User.Name, c.Raiting.Val, c.TextTemplate, c.Id);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                Done($"Loaded comments from {usrComms.Data.User.Name}. Overall rating {usrComms.Data.UserDetails.CountRaiting}. Last online: {usrComms.Data.UserDetails.DateLastVisit.ToString("dd MMM yyyy HH:mm")}");
            }
            catch (Exception ex)
            {
                Done("An error occured: " + ex.Message);
                Debug.WriteLine(ex);
            }
        }
        #endregion

        #region Web stuff
        string VotePost(string commentid, Vote vote, string token)
        {
            string v = (vote == Vote.down) ? "down" : "up";

            var request = (HttpWebRequest)WebRequest.Create("https://web.tolstoycomments.com/api/raiting/" + v + "?token=" + HttpUtility.UrlEncode(token, UnicodeEncoding.UTF8));
            request.CookieContainer = cookieContainer;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "*/*";
            request.UserAgent = userAgent;

            var postData = "{\"comment_id\":" + Uri.EscapeDataString(commentid);
            postData += ",\"token\":\"" + HttpUtility.UrlDecode(token, UnicodeEncoding.UTF8) + "\"}";

            request.Headers.Add("Accept-Language", "en-US,en;q=0.9");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");

            var data = Encoding.UTF8.GetBytes(postData);
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
            response.Close();
            return res;
        }

        private string WebGET(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            //request.CookieContainer = cookie;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "*/*";
            request.UserAgent = userAgent;

            request.Headers.Add("Accept-Language", "en-US,en;q=0.9");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");

            var response = (HttpWebResponse)request.GetResponse();
            string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
            response.Close();
            return res;
        }
        #endregion

        #region ListView Related
        private void SaveHistory(string commId)
        {
            try
            {
                string str = commId + "|" + VoteMode();
                string[] file = null;
                if (!File.Exists(historyFilePath))
                    File.CreateText(historyFilePath);
                else
                    file = File.ReadAllLines(historyFilePath);

                var historyList = new List<string>();
                if (file != null)
                    historyList = new List<string>(file);

                var match = historyList.FirstOrDefault(stringToCheck => stringToCheck.Contains(commId));
                if (match == null)
                    historyList.Add(str);
                else
                {
                    int index = historyList.FindIndex(s => s.Contains(commId));
                    if (index != -1)
                        if (!historyList[index].Equals(str))
                            historyList[index] = str;
                }

                File.WriteAllLines(historyFilePath, historyList);
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        private void AddToList(string[] historyList, DateTime dateTime, int id, string nick, int vote, string comm, int commid)
        {
            ListViewItem list = new ListViewItem(dateTime.ToString("dd MMM yy HH:mm"));
            list.SubItems.Add(id.ToString());
            list.SubItems.Add(nick);
            list.SubItems.Add(vote.ToString());
            list.SubItems.Add(HttpUtility.HtmlDecode(Str.StripHTML(comm)));
            list.SubItems.Add(commid.ToString());

            if (historyList != null)
            {
                foreach (string s in historyList)
                {
                    if (s.Contains(commid.ToString()))
                    {
                        string voteStr = s.Split('|')[1];
                        if (voteStr.Equals("up"))
                            list.ForeColor = Color.ForestGreen;
                        else if (voteStr.Equals("down"))
                            list.ForeColor = Color.Firebrick;
                    }
                }
            }

            commListView.Items.Add(list);
        }

        private void viewComment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (commListView.SelectedItems.Count == 1)
            {
                if (commListView.Items.Count > 0)
                {
                    for (int i = 0; i < commListView.Items.Count; i++)
                    {
                        if (commListView.Items[i].Selected == true)
                        {
                            commentViewer.Text = commListView.Items[i].SubItems[2].Text + ": " + commListView.Items[i].SubItems[4].Text;
                        }
                    }
                }
            }
        }

        //View profile
        private void viewProfileComments_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            isCancelled = true;

            Run($"Loading profile comments...");

            goBackBtn.Enabled = true;
            string userId = null;

            if (commListView.Items.Count > 0)
            {
                for (int i = 0; i < commListView.Items.Count; i++)
                {
                    if (commListView.Items[i].Selected == true)
                    {
                        userId = commListView.Items[i].SubItems[1].Text;
                    }
                }
            }

            LoadProfileComments(userId);
        }
        #endregion

        #region Update
        private async void CheckUpdate()
        {
            string remoteVer = null;
            await Task.Factory.StartNew(() =>
            {
                remoteVer = UpdateUtils.RemoteVersion();
            });

            if (!UpdateUtils.RemoteVersion().Contains(ProductVersion))
                Log("There is a new update available. Please check repo https://github.com/VanHoevenTR/Russia-Today-Vote-Botting");
            else
                Log("No update available");
        }
        #endregion

        #region Form control handlers
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void Run(string text)
        {
            isRunning = true;
            startBtn.Enabled = false;
            massVoteCommBtn.Enabled = false;
            loadComms.Enabled = false;
            editTokens.Enabled = false;
            statusLbl.Text = text;
            Log(text);
            progressBar.Style = ProgressBarStyle.Marquee;
        }

        private void Done(string text = "Done")
        {
            isRunning = false;
            isCancelled = false;
            startBtn.Enabled = true;
            massVoteCommBtn.Enabled = true;
            loadComms.Enabled = true;
            editTokens.Enabled = true;
            statusLbl.Text = text;
            Log(text);
            progressBar.Style = ProgressBarStyle.Continuous;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            logTextBox.SelectionStart = logTextBox.Text.Length;
            logTextBox.ScrollToCaret();
        }

        private void logTextBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void Log(string text)
        {
            if (logTextBox.InvokeRequired)
            {
                Invoke(new Action(delegate ()
                {
                    logTextBox.AppendText(text + Environment.NewLine);
                }));
            }
            else
            {
                logTextBox.AppendText(text + Environment.NewLine);
            }
        }
        #endregion
    }
}
