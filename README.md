# Russia Today Vote Botting by VanHoevenTR

![Tools2](https://user-images.githubusercontent.com/70694330/172657377-ddb88ad5-76e2-4f99-af90-955ad02e703c.png)

_Tool in action_

There are downvoter bots problem who kept mass downvoting pro-ukrainian comments, it is created by Z__Z__Z [PROOF](https://github.com/VanHoevenTR/Russia-Today-Vote-Botting/tree/main/Images/Proof). I have been hit by these bots and got my account restricted, so I decided to create my own bots written in C# and fight back :D. I'm not good in C#, so code might be messy

Because I do not know how to bypass invisible captcha, I use tokens. It can be easly obtained from the browser after logged in (Explained below). PLEASE TELL ME if you know how to bypass captcha. We could do more fun stuff :D

**I need your help to make it known. Please share this to your pro-ukrainian friends, your favorite pro-ukrainian hackers, IT army of Ukraine, hacking forums, Twitter, or anywhere.**

I know pro-ruZZian can use this too, so always be prepared with vote fighting. 

# Features
- Load comments from article url or user ID
- Mass vote on user profile
- Mass voting a comment
- Cancel operation
- Edit tokens text
- Downvote or upvote
- Max voting
- Delay voting
- Randomize voting
- Voted comment history. Comment will be highlighted green if upvoted, and red if downvoted
- Preview comment
- Update check
- Support rt.com, russian.rt.com and arabic.rt.com (actualidad.rt.com is using Disqus commenting system by United States, but I will NOT SUPPORT it. Instead, please ask Disqus to revoke RT from using it.)

# How to use the tool

Really simple to use

Download the tool first: 
[![](https://img.shields.io/github/downloads/VanHoevenTR/Russia-Today-Vote-Botting/total)](https://github.com/VanHoevenTR/Russia-Today-Vote-Botting/releases)

You might want to disable antivirus or whitelist first because they are dumb to detect it as virus. I don't know why, I don't make virus. If you think it's a virus, see the source code thoroughly.

Run

Read below how to obtain token

Click Edit Text to add your tokens

Have fun :D

# How to obtain token

Important: Use dummy info and throw away emails, you don't need to be verified. Do not use inappropriate
 names, use good names, RT always check newly created accounts. If you kept getting banned after creating accounts, clear 
cookies of tolstoycomments.com and create another account. Using VPN can probably increase risks of detecting bot and banning. Tolstoy does not detect vote botting at the moment

Go to rt.com, visit any articles and login with your account.

Right click anywhere, click **Inspect**, 

Go to **Application**, **Local Storage**, **tolstoycomments** and here you can see your token (I use Google Chrome but it is very similar on other browsers)

![](Images/Token.png)

Open my tool, click **Edit Text**, Paste one or more tokens each lines

Log out and login/create with another account. Log out will not make token invalid (Which is good)

Repeat.

# Inspect traffic

I used Fiddler Classic https://www.telerik.com/download/fiddler

You can also do that via browser's inspector

# Disclaimer

This is just my hobby project, it is not illegal to botting but use at your own risk. This tool does not spam comments. Spam is boring really, and useless since AI and moderators are active
